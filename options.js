// requires tea.js

var config;

function E(a  ) { console.log('#E : cancelor : options : ' + a) }
function I(a,b) { console.log('#I : cancelor : options', a,b) }
function Q(q) { return document.body.querySelector(q) }

function load(){
	browser.storage.local.get(['config']).then(
		(o)=>{
			config={
				cb_can : true,
				cb_cop : true
			};
			if(o['config'])
				config=o['config'];

			Q('#app').value = config['app'] || '';
			Q('#cb_can').checked = config['cb_can'] || false;
			Q('#cb_cop').checked = config['cb_cop'] || false;
			Q('#cb_app').checked = config['cb_app'] || false;

			I('loaded',config);
		},
		()=>{ E('load : error') },
	)
}

function save(){
	config['app'] = Q('#app').value;
	config['cb_can'] = !!Q('#cb_can').checked;
	config['cb_cop'] = !!Q('#cb_cop').checked;
	config['cb_app'] = !!Q('#cb_app').checked;

	browser.storage.local.set({'config':config}).then(
		()=>{
			I('saved',config)
		},
		()=>{ E('save : error') },
	)
}

Q('input[type=button]').onclick = save;

load();
