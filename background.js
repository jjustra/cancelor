var intercept_b = 0;
var config = {};

function render()
{

 if (!intercept_b) {

  browser.browserAction.setBadgeText({text:'off'})
  browser.browserAction.setBadgeBackgroundColor({color:'#333'})

 } else {

  browser.browserAction.setBadgeText({text:'on'})
  browser.browserAction.setBadgeBackgroundColor({color:'#3e3'})

 }

}

browser.storage.onChanged.addListener((o,area) => {
 if(area=='local' && o['config'])
  config = o['config'].newValue;
});

browser.downloads.onCreated.addListener((itm)=>{

 // Refresh
 render();

 // Cancelor is turned off
 if (!intercept_b) return;

 // stop download
 if (config.cb_can)
  browser.downloads.cancel(itm.id);

 // Write url to clipboard
 if (config.cb_cop)
  navigator.clipboard.writeText(itm.url);

 // Send url to external app
 if (config.cb_app && config.app)

  // Notify : sending
  browser.browserAction.setBadgeText({text:'...'})
  browser.browserAction.setBadgeBackgroundColor({color:'#e83'})

  browser.runtime.sendNativeMessage(config.app, itm.url).then((resp)=>{

   // Notify : success
	browser.browserAction.setBadgeText({text:resp.toString()})
	if (intercept_b)
		browser.browserAction.setBadgeBackgroundColor({color:'#3e3'})

  },(e)=>{

   // Notify : error
	browser.browserAction.setBadgeText({text:'err'})
	if (intercept_b)
		browser.browserAction.setBadgeBackgroundColor({color:'#e33'})

  });

});

browser.browserAction.onClicked.addListener((tab) => {

 intercept_b = !intercept_b;
 render();

});

browser.storage.local.get(['config']).then(
	(o)=>{
		if(o['config'])
			config = o['config'];
	},
	()=>{},
)

render();
